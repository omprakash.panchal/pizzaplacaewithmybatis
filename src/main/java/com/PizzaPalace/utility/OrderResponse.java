package com.PizzaPalace.utility;

import com.PizzaPalace.model.Order;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderResponse {

    Boolean success ;
    String message;
    Order order;
    Object PizzaList;
    Object updatedPizzaList;
    Object data;
}
