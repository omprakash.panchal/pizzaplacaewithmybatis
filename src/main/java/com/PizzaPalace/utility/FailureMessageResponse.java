package com.PizzaPalace.utility;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FailureMessageResponse {


    private String message;

    private boolean success;

    private ErrorMesage error;
}
