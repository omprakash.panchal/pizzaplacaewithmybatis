package com.PizzaPalace.utility;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorMesage {
    private  int code;
    String message;

}
