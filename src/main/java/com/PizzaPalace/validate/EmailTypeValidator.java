package com.PizzaPalace.validate;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EmailTypeValidator implements ConstraintValidator<EmailValid, String> {
    private Pattern pattern;
    private Matcher matcher;

    private Logger logger = LoggerFactory.getLogger(EmailTypeValidator.class);


    private static final String EMAIL_PATTERN =
            "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";


    @Override
    public void initialize(EmailValid constraintAnnotation) {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {

        logger.info("Message from isValid : {} ", email);
        if (email == null || email.isBlank()) {
            return true; // Allow null or empty value, handle required validation separately
        }
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}


