package com.PizzaPalace.services;

import com.PizzaPalace.mapper.PizzaMapper;
import com.PizzaPalace.model.Pizza;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PizzaService {

    @Autowired
    private PizzaMapper pizzaMapper;

    // Create
    public Long createPizza(Pizza pizza) {
        Long id = pizzaMapper.insertPizza(pizza);

        return id;
    }

    //update

    public Pizza updatePizzaByID(Long pizzaId , Pizza pizza)
    {

        pizzaMapper.updatePizza(pizza);
        return pizzaMapper.getPizzaById(pizza.getPizzaId());

    }

    //get Pizza By Id

    public Pizza getPizza(Long pizzaId){
        Pizza pizzaById= pizzaMapper.getPizzaById(pizzaId);
        return  pizzaById;
    }


    //delete By ID
    public Boolean deletePizza(long id)
    {
        Boolean deletedId= pizzaMapper.deletePizzaById(id);
        return deletedId;
    }

    // Get All

    public List<Pizza> getAll()
    {
        List<Pizza> allPizza = pizzaMapper.getAllPizza();

        return allPizza;
    }



}
