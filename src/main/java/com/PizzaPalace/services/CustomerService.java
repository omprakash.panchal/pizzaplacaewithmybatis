package com.PizzaPalace.services;

import com.PizzaPalace.exception.ResourceNotFoundException;
import com.PizzaPalace.mapper.CustomerMapper;
import com.PizzaPalace.model.*;
import com.PizzaPalace.utility.ErrorMesage;
import com.PizzaPalace.utility.FailureMessageResponse;
import com.PizzaPalace.utility.MessageResponse;
import com.PizzaPalace.utility.OrderResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.management.BadAttributeValueExpException;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class CustomerService {

    @Autowired
    private CustomerMapper customerMapper;


    // Create

    public Long createCustomer(Customer customer) {
        Long id = customerMapper.insertCustomer(customer);

        return id;
    }

    //get Customer By Id
    public Customer getCustomer(Long id) {
        log.info("inside getCustomer from customer Service");
        Customer customer = customerMapper.getCustomerById(id);
        System.out.println(customer);
        return customer;
    }

    //delete By ID
    public Boolean deleteCustomer(long id) {
        return customerMapper.deleteCustomerById(id);
    }


    // Get All

    public List<Customer> getAll() {
        List<Customer> allCustomers = customerMapper.getAllCustomers();
        return allCustomers;
    }

    // Update Customer

    public Customer updateCustomerByID(Long custId, Customer customer) {

        customerMapper.updateCustomer(customer);
        return customerMapper.getCustomerById(customer.getCustId());

    }


    public Object logInCustomer(LogInRequest loginRequest) {
        System.out.println("inside Login Customer From Service");
        log.info("inside Customer Service");

        Customer customer = customerMapper.getCustomerByEmail(loginRequest.getEmail());
        System.out.println(loginRequest);

        if (loginRequest.getEmail() == null  || customer == null) {
            log.info("inside null email");
            FailureMessageResponse failedResponse = FailureMessageResponse.builder()
                    .success(false)
                    .message("Failed To Log In")
                    .error(ErrorMesage.builder().message("Not Found Register First..!!").code(404).build())
                    .build();

            return failedResponse;
        }



       if (loginRequest.getEmail().equals(customer.getEmail()) && loginRequest.getPassword().equals(customer.getPassword())) {
            MessageResponse successResponse = MessageResponse.builder()
                    .success(true)
                    .message("Logged In Successfully")
                    .data(customer)
                    .httpStatus(HttpStatus.OK)
                    .build();

            return successResponse;
        } else {
            FailureMessageResponse failedResponse = FailureMessageResponse.builder()
                    .success(false)
                    .message("Failed To Log In")
                    .error(ErrorMesage.builder().message(" Password Incorrect..!!").code(404).build())
                    .build();

            return failedResponse;
        }
    }



}
