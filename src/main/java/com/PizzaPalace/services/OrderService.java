package com.PizzaPalace.services;


import com.PizzaPalace.mapper.OrderMapper;


import com.PizzaPalace.model.Order;
import com.PizzaPalace.model.OrderLine;
import com.PizzaPalace.model.Pizza;
import com.PizzaPalace.utility.OrderResponse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class OrderService {


    @Autowired
    private OrderMapper orderMapper;

    public OrderResponse createOrder(Order order) {
        System.out.println("inside create Order");

        log.info("inside Order Service");


        Order createdOrder = Order.builder().orderDate(new Date()).custId(order.getCustId()).Status(order.getStatus()).totalAmount(order.getTotalAmount()).deliveryAddress(order.getDeliveryAddress()).build();
        // Save Order
        orderMapper.createOrder(createdOrder);
        Long generatedId = createdOrder.getOrderId();


        // Iterate over the list of pizzas in the order
        for (Pizza pizza : order.getPizzaList()) {

            // Create an OrderLine entity for each pizza
            OrderLine orderLine = new OrderLine();
            orderLine.setOrderId(generatedId);
            orderLine.setPizzaId(pizza.getPizzaId());
            orderLine.setQuantity(pizza.getQuantity());
            orderLine.setSize(pizza.getSize());
            orderLine.setTotalLineAmount(pizza.getPrice());
            System.out.println(pizza);
            // Save Order Line
            orderMapper.createOrderLine(orderLine);

        }

        System.out.println(createdOrder);
        // Response Generating For Order
        OrderResponse orderResponse = OrderResponse.builder().success(true).message("Order Created SuccesFully").order(createdOrder).PizzaList(order.getPizzaList()).build();

        log.info("Order Placed Successfully");

        return orderResponse;
    }


    //new Create or update Handler


    public OrderResponse createOrUpdateOrder(Order order ) {
        System.out.println("inside createOrUpdateOrder");
        log.info("inside Order Service");
        System.out.println(order);
        if (order.getOrderId() == null) {
            log.info("inside new Create Order Service");
            // This is a new order, so create it
            Order createdOrder = Order.builder()
                    .orderDate(new Date())
                    .custId(order.getCustId())
                    .Status(order.getStatus())
                    .totalAmount(order.getTotalAmount())
                    .deliveryAddress(order.getDeliveryAddress())
                    .build();

            // Save the new order
            orderMapper.createOrder(createdOrder);
            Long generatedId = createdOrder.getOrderId();

            // Iterate over the list of pizzas in the order and create order lines
            for (Pizza pizza : order.getPizzaList()) {
                OrderLine orderLine = new OrderLine();
                orderLine.setOrderId(generatedId);
                orderLine.setPizzaName(pizza.getPizzaName());
                orderLine.setPizzaId(pizza.getPizzaId());
                orderLine.setQuantity(pizza.getQuantity());
                orderLine.setSize(pizza.getSize());
                orderLine.setTotalLineAmount(pizza.getPrice());

                // Save Order Line
                orderMapper.createOrderLine(orderLine);
            }

            // Response for new order creation
            OrderResponse orderResponse = OrderResponse.builder()
                    .success(true)
                    .message("Order Created Successfully")
                    .order(createdOrder)
                    .PizzaList(order.getPizzaList())
                    .build();

            log.info("Order Placed Successfully");

            return orderResponse;
        } else {
            // This is an existing order, so update it
            Order existingOrder = orderMapper.getOrderById(order.getOrderId());
            log.info("inside new Update Order Service");
            if (existingOrder == null) {
                // Handle the case where the order with the given orderId does not exist
                // You can throw an exception or return an error response here
                return OrderResponse.builder()
                        .success(false)
                        .message("Order with ID " + order.getOrderId() + " not found")
                        .build();
            }

            // Update the order details
            existingOrder.setCustId(order.getCustId());
            existingOrder.setStatus(order.getStatus());
            existingOrder.setTotalAmount(order.getTotalAmount());
            existingOrder.setDeliveryAddress(order.getDeliveryAddress());

            // Update the order in the database
            orderMapper.updateOrder(existingOrder);

            // Delete existing order lines
            orderMapper.deleteOrderLinesByOrderId(order.getOrderId());

            // Create new order lines
            for (Pizza pizza : order.getPizzaList()) {
                OrderLine orderLine = new OrderLine();

                orderLine.setOrderId(order.getOrderId());
                orderLine.setPizzaName(pizza.getPizzaName());
                orderLine.setQuantity(pizza.getQuantity());
                orderLine.setSize(pizza.getSize());
                orderLine.setTotalLineAmount(pizza.getPrice());

                // Save Order Line
                orderMapper.createOrderLine(orderLine);
            }

            // Response for order update
            OrderResponse orderResponse = OrderResponse.builder()
                    .success(true)
                    .message("Order Updated Successfully")
                    .order(existingOrder)
                    .updatedPizzaList(order.getPizzaList())
                    .build();

            log.info("Order Updated Successfully");

            return orderResponse;
        }
    }


    //Get All

    public List<Order> getAll() {
        List<Order> allOrders = orderMapper.getAllOrders();

        return allOrders;
    }


    // Deleye Order By Id

    public void deleteOrder(Long orderId) {
        orderMapper.delete(orderId);
    }


    public Order getOrder(Long orderId) {

        return orderMapper.getOrderById(orderId);

    }
}
