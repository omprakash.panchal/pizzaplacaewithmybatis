package com.PizzaPalace.model;



import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Pizza {


    @NotBlank(message = "Pizza Name is Required")
    @Size(min = 3, max = 20, message = " Enter Size more Than 3 Chars!!")
    private String pizzaName;

    @NotBlank(message = "Description is Required")
    @Size(min = 3, max = 50, message = " Enter Size more Than 3 Chars!!")
    private String description;

    @NotBlank(message = "Type is Required")
    @Size(min = 3, max = 20, message = " Enter Size more Than 3 Chars!!")
    private String pizzaType;


    private String imageUrl;

    private Double priceRegSize;

    private String size;

    private Integer quantity;


    private Double priceMSize;


    private Double priceLSize;

    public List<OrderLine> orderLine;

    private Integer price;

    private Long pizzaId;


}
