package com.PizzaPalace.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Order {


    private Long orderId;

    @NotBlank(message = "Status is Required")
    private String Status ;


    private Date orderDate ;


    private double totalAmount ;


    @NotBlank(message = "Address is Required")
    @Size(min = 3, max = 20, message = " Enter Size more Than 3 Chars!!")
    private String deliveryAddress;

    private Long custId;


  private List<Pizza> pizzaList;




}
