package com.PizzaPalace.model;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class LogInRequest {

    private String email;

    private String password;
}
