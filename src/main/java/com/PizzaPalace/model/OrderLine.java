package com.PizzaPalace.model;


import lombok.*;



@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder

public class OrderLine {

    private Long lineId;

    private Long orderId;

    private String size;

    private String pizzaName;

    private Integer quantity;

    private Integer totalLineAmount;

    private Long pizzaId;




}
