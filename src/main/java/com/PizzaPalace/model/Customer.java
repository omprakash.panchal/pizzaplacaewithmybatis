package com.PizzaPalace.model;

import com.PizzaPalace.validate.EmailValid;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;


import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Customer {


    private Long custId;


    @NotBlank(message = "First Name  is Required")
    @Size(min = 3, max = 20, message = " Enter Size more Than 3 Chars!!")
    private String firstName;


    @NotBlank(message = "Last Name is Required")
    @Size(min = 3, max = 20, message = " Enter Size more Than 3 Chars!!")
    private String lastName;

    @NotNull
    @EmailValid(message = "invalid email !!")
    private String email;

    @Size(min = 3, max = 20, message = " Enter Size more Than 3 Chars!!")
    @NotBlank(message = "PassWord is Required")
    private String password;

    @NotBlank(message = "Name is Required")
    @Size(min = 3, max = 20, message = " Enter Size more Than 3 Chars!!")
    private String contactNumber;


    @NotBlank(message = "Name is Required")
    @Size(min = 3, max = 50, message = " Enter Size more Than 3 Chars!!")
    private String address;

    public List<Order> orders;


}
