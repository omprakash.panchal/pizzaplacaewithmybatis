package com.PizzaPalace.exception;

public class InternalServerErrorException extends RuntimeException {
    public InternalServerErrorException() {
        super("Internal Server Error Exception Occured");
    }

    public InternalServerErrorException(String message) {
        super(message);
    }


}
