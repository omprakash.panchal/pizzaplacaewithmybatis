package com.PizzaPalace.exception;


import com.PizzaPalace.utility.ErrorMesage;
import com.PizzaPalace.utility.FailureMessageResponse;
import com.PizzaPalace.utility.MessageResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {

    //Resource Not Found Exception
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<MessageResponse> resourceNotFoundExceptionHanler(ResourceNotFoundException ex) {
        MessageResponse response = MessageResponse.builder().message(ex.getMessage()).success(false).httpStatus(HttpStatus.NOT_FOUND).build();
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }


    //Method Level Validation Exception
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<FailureMessageResponse> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        FailureMessageResponse response = FailureMessageResponse.builder().message("Failed to Add Data").success(false).error(ErrorMesage.builder().code(400).message(errors.toString()).build()).build();
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


    //Internal Server Exception

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<MessageResponse> handleInternalServerError(InternalServerErrorException ex) {
        MessageResponse response = MessageResponse.builder().message(ex.getMessage()).success(false).build();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }


    // Not Acceptable Exception


    @ExceptionHandler(NotAcceptableException.class)
    public ResponseEntity<MessageResponse> handleNotAcceptableException(NotAcceptableException ex) {

        MessageResponse response = MessageResponse.builder().message(ex.getMessage()).success(false).build();

        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
    }
}
