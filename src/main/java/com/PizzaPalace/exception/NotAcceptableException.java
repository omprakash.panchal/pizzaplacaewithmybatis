package com.PizzaPalace.exception;


import lombok.Builder;


@Builder
public class NotAcceptableException extends RuntimeException {
    public NotAcceptableException() {
        super("Resource Not Found");
    }

    public NotAcceptableException(String message) {
        super(message);
    }
}
