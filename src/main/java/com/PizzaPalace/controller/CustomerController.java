package com.PizzaPalace.controller;


import com.PizzaPalace.mapper.CustomerMapper;
import com.PizzaPalace.model.Customer;
import com.PizzaPalace.model.LogInRequest;
import com.PizzaPalace.model.Order;
import com.PizzaPalace.services.CustomerService;
import com.PizzaPalace.utility.ErrorMesage;
import com.PizzaPalace.utility.FailureMessageResponse;
import com.PizzaPalace.utility.MessageResponse;
import com.PizzaPalace.utility.OrderResponse;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/customers")
@Slf4j
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerMapper customerMapper;


//    private Logger logger = LoggerFactory.getLogger(CustomerController.class);

    //Create Customer
    @PostMapping
    public ResponseEntity<MessageResponse> createCustomer(@Valid @RequestBody Customer customer) {
        log.info("Inside createCustomer Handler of CustomerController");

        Long id = customerService.createCustomer(customer);
        System.out.println(id);
        MessageResponse response = MessageResponse.builder().message("Customer Created Successfully").success(true).httpStatus(HttpStatus.OK).data(customer).build();
        log.info("Customer Created Successfully");
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }


    // Get By Id

    @GetMapping("/{custId}")
    public ResponseEntity<Object> getCustomerById(@PathVariable Long custId) {
        log.info("Inside getCustomerByID Handler of CustomerController");

        Customer customer = customerService.getCustomer(custId);


        if (customer == null) {

            ErrorMesage response = ErrorMesage.builder().
                    code(400)
                    .message("Not Found")
                    .build();

            FailureMessageResponse messageResponse = FailureMessageResponse
                    .builder()
                    .message("User Not Found From This ID")
                    .success(false)
                    .error(response).build();
            log.info("User Not Found From This ID");
            return ResponseEntity.internalServerError().body(messageResponse);
        } else {

            MessageResponse response = MessageResponse.builder()
                    .success(true)
                    .message("Customer Fetched Successfully..!")
                    .data(customer)
                    .httpStatus(HttpStatus.OK)
                    .build();
            log.info("Customer Fetched Successfully..!");
            return new ResponseEntity<>(response, HttpStatus.OK);


        }


    }

    //LogIn Customer
    @PostMapping("/email")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<?> login(@RequestBody LogInRequest request) {
        log.info("Inside Log In customer");
        System.out.println(request);
        Object response = customerService.logInCustomer(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);

    }


    //get All Customer

    @GetMapping
    public ResponseEntity<MessageResponse> getAllCustomer() {
        log.info("Inside GetAll Customer of Customer Controller");
        MessageResponse response = MessageResponse.builder().message("Customer Fetched Successfully ..!!").success(true).httpStatus(HttpStatus.OK).data(customerService.getAll()).build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // Delete Customer
    @DeleteMapping("/{custId}")
    public ResponseEntity<Object> deleteCustomer(@PathVariable Long custId) {
        log.info("Inside deleteCustomer Handler of CustomerController");

        Boolean response = customerService.deleteCustomer(custId);


        if (!response) {

            ErrorMesage errorMesage = ErrorMesage.builder().
                    code(400)
                    .message("Not Found")
                    .build();

            FailureMessageResponse messageResponse = FailureMessageResponse
                    .builder()
                    .message("User Not Found From This ID")
                    .success(false)
                    .error(errorMesage).build();
            return ResponseEntity.internalServerError().body(messageResponse);
        } else {

            MessageResponse messageResponse = MessageResponse.builder()
                    .success(true)
                    .message("Customer Deleted Successfully..!")
                    .data(response)
                    .httpStatus(HttpStatus.OK)
                    .build();
            return new ResponseEntity<>(messageResponse, HttpStatus.OK);

        }
    }


    //Update


    @PutMapping("/{custId}")
    public ResponseEntity<MessageResponse> updateCustomer(@Valid @PathVariable Long custId, @RequestBody Customer customer) {
        customerService.updateCustomerByID(custId, customer);
        MessageResponse response = MessageResponse.builder().message("Customer Added Successfully ..!!").success(true).httpStatus(HttpStatus.OK).data(customer).build();
        log.info("Customer Added Successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

}


