package com.PizzaPalace.controller;

import com.PizzaPalace.model.Customer;
import com.PizzaPalace.model.Order;
import com.PizzaPalace.services.OrderService;
import com.PizzaPalace.utility.ErrorMesage;
import com.PizzaPalace.utility.FailureMessageResponse;
import com.PizzaPalace.utility.MessageResponse;
import com.PizzaPalace.utility.OrderResponse;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;


    //Create Order
    @PostMapping
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<OrderResponse> createOrder(@Valid @RequestBody Order orders) {
        log.info("Inside Create Order In Order Controller");
        OrderResponse orderResponse = orderService.createOrUpdateOrder(orders);
        return new ResponseEntity<>(orderResponse, HttpStatus.CREATED);

    }

    //GEt All Order
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping
    public ResponseEntity<OrderResponse> getAllOrder() {

        OrderResponse orderResponse = OrderResponse.builder().success(true).message("Order Fetched..!!").data(orderService.getAll()).build();
        return new ResponseEntity<>(orderResponse, HttpStatus.OK);
    }

    // Delete Order

    @DeleteMapping("/{orderId}")
    public ResponseEntity<OrderResponse> deleteOrder(@PathVariable Long orderId) {
        log.info("Inside Delete Order");
        orderService.deleteOrder(orderId);
        OrderResponse orderDeleted = OrderResponse.builder().success(true).message("Order Deleted").data("Deleted Order ID : -" + orderId).build();
        return new ResponseEntity(orderDeleted, HttpStatus.OK);
    }


    // Get Order By Id
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{orderId}")
    public ResponseEntity<Object> getCustomerById(@PathVariable Long orderId) {
        log.info("Inside getOrderByID Handler of OrderHandler");

        Order order = orderService.getOrder(orderId);
        System.out.println("from Order Controller " + order);

        if (order == null) {

            ErrorMesage response = ErrorMesage.builder().
                    code(400)
                    .message("Not Found")
                    .build();

            FailureMessageResponse messageResponse = FailureMessageResponse
                    .builder()
                    .message("Order Not Found From This ID")
                    .success(false)
                    .error(response).build();
            return ResponseEntity.internalServerError().body(messageResponse);
        } else {

            MessageResponse response = MessageResponse.builder()
                    .success(true)
                    .message("Order Fetched Successfully..!")
                    .data(order)
                    .httpStatus(HttpStatus.OK)
                    .build();
            return new ResponseEntity<>(response, HttpStatus.OK);


        }


    }


}
