package com.PizzaPalace.controller;


import com.PizzaPalace.model.Pizza;
import com.PizzaPalace.services.PizzaService;
import com.PizzaPalace.utility.ErrorMesage;
import com.PizzaPalace.utility.FailureMessageResponse;
import com.PizzaPalace.utility.MessageResponse;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pizza")
@Slf4j
public class PizzaController {
    @Autowired
    private PizzaService pizzaService;


    // Create
    @PostMapping
    public ResponseEntity<MessageResponse> createPizza(@Valid @RequestBody Pizza pizza) {
        log.info("inside Create Pizza");
        Long id = pizzaService.createPizza(pizza);
        MessageResponse response = MessageResponse.builder().message("Pizza Created Successfully..!").success(true).httpStatus(HttpStatus.CREATED).data(pizza).build();
        log.info(" Created Pizza");
        return new ResponseEntity<>(response, HttpStatus.CREATED);

    }


    //Update Pizza

    @PutMapping("/{pizzaId}")
    public ResponseEntity<Object> updatePizza(@PathVariable Long pizzaId, @Valid @RequestBody Pizza pizza) {
        log.info("inside Update Pizza");
        System.out.println(pizzaId + " " + pizza);
        pizza.setPizzaId(pizzaId);
        Pizza updatedPizza = pizzaService.updatePizzaByID(pizzaId, pizza);
        System.out.println(updatedPizza);
        if (updatedPizza == null) {

            ErrorMesage response = ErrorMesage.builder().
                    code(400)
                    .message("Pizza Not Found")
                    .build();

            FailureMessageResponse messageResponse = FailureMessageResponse
                    .builder()
                    .message("Pizza Not Found From This ID")
                    .success(false)
                    .error(response).build();
            return ResponseEntity.internalServerError().body(messageResponse);
        } else {

            MessageResponse response = MessageResponse.builder()
                    .success(true)
                    .message("Pizza Fetched Successfully..!")
                    .data(updatedPizza)
                    .httpStatus(HttpStatus.OK)
                    .build();
            return new ResponseEntity<>(response, HttpStatus.OK);


        }


    }


    //Get All Pizzas
    @GetMapping
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<MessageResponse> getAllPizzas() {
        log.info("inside GEtAll Pizza");
        MessageResponse response = MessageResponse.builder().message("All pizzas..!").success(true).httpStatus(HttpStatus.OK).data(pizzaService.getAll()).build();

        return new ResponseEntity<>(response, HttpStatus.OK);


    }


    // Delete Pizza
    @DeleteMapping("/{pizzaId}")
    public ResponseEntity<Object> deletePizza(@PathVariable Long pizzaId) {
        log.info("inside Delete Pizza");
        Boolean response = pizzaService.deletePizza(pizzaId);

        if (!response) {

            ErrorMesage errorMesage = ErrorMesage.builder().
                    code(400)
                    .message("Not Found")
                    .build();

            FailureMessageResponse messageResponse = FailureMessageResponse
                    .builder()
                    .message("Pizza Not Found From This ID")
                    .success(false)
                    .error(errorMesage).build();
            return ResponseEntity.internalServerError().body(messageResponse);
        } else {

            MessageResponse messageResponse = MessageResponse.builder()
                    .success(true)
                    .message("Pizza Deleted Successfully..!")
                    .data("deleted pizza Id " + pizzaId)
                    .httpStatus(HttpStatus.OK)
                    .build();
            return new ResponseEntity<>(messageResponse, HttpStatus.OK);
        }


    }


}
