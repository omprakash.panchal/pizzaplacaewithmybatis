package com.PizzaPalace.mapper;

import com.PizzaPalace.model.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CustomerMapper {

    //create
    Long insertCustomer(@Param("customer") Customer customer);

    // Get By Id
    Customer getCustomerById(Long id);


    // Get By Email
    Customer getCustomerByEmail(String email);

    //Get All
    List<Customer> getAllCustomers();

    //Delete By Id
    public Boolean deleteCustomerById(Long id);
    
    //update Customer

    void updateCustomer(Customer customer);


}
