package com.PizzaPalace.mapper;

import com.PizzaPalace.model.Pizza;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PizzaMapper {

    //create
    Long insertPizza(@Param("pizza") Pizza pizza);

    // Get By Id
    Pizza getPizzaById(Long pizzaId);

    //update
    void updatePizza( Pizza pizza);

    //Delete By Id
    public Boolean deletePizzaById(Long id);

    //Get All
    List<Pizza> getAllPizza();
}
