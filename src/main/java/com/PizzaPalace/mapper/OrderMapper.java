package com.PizzaPalace.mapper;


import com.PizzaPalace.model.Customer;
import com.PizzaPalace.model.Order;
import com.PizzaPalace.model.OrderLine;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMapper {

    // Create Order
    Long createOrder(@Param("order") Order orders);

    //update Order
    void updateOrder(Order order);

    //deleting Existing OrderLIne || created One
    void deleteOrderLinesByOrderId(Long orderId);

    // Create OrderLine
    Long createOrderLine(@Param("orderline") OrderLine orderLine);

    //Get All
    List<Order> getAllOrders();

    //Delete Order By ID

    void delete(Long orderId);

    //getOrderByID

    Order getOrderById(Long orderId);
}
